Get Started with Web IDE
========================

.. important::

  Rell Web IDE is available at https://rellide-staging.chromia.dev/

Upon entering, you will see an interface similar to the image below.

Click **Create Module** button:

|Create Module Button|

This will open a modal element where you can specify the name of the Module and the Language used (in this example: Rell).

For convenience you can include template and test code.

|Create Options|

Click the blue "Create" button. The screen will now be filled with code.

|Template|

Browser
    To the left bar is the Browser. You can use it to work with several examples.

Editor
    Inside the central element on the right the editor filled with a template of source code.

Buttons
    On top of the Editor there is a button to "Start Node" (the green "Play" icon), don't press that one yet.
    There is also a button "Run tests" (the gray "bug" icon).

Hello World!
~~~~~~~~~~~~

As a minimal first application, you can make a Hello World example with a focus on Ukraine.

If you checked the *use template* box and look at editor section on the top, you will see this code as a template:


.. code-block:: rell

    entity city { key name; }

    operation insert_city (name) {
        create city (name);
    }

This is a small registry of cities.

Don't worry about the detail of this code yet, we will come to them in a bit. For now, let's confirm that our code template is working properly.

In order to run the code we need a test in javascript. If you switch to the ``Hello.js`` test file, you will see it's filled with some test written in javascript:

.. code-block:: javascript

    const tx = gtx.newTransaction([user.pubKey]);

    tx.addOperation('insert_city', "Kiev");

    tx.sign(user.privKey, user.pubKey);

    return tx.postAndWaitConfirmation();

Click the 'Run tests' button, and a green message will appear.

|Success|


Congratulations! After all this work, we suggest that you put "Relational Blockchain" on your CV.

Where to go next?
~~~~~~~~~~~~~~~~~

Next step is to learn about what those Rell code actually mean in the :doc:`Rell Basics </rell-basics/index>` section.

But if you prefer learning by example, you can choose to start with one of our :doc:`Example Projects </examples/index>` instead.

.. |Create Module Button| image:: create-module-button.png
  :width: 600
  :alt: Where to create a new module

.. |Create Options| image:: create-rell-module.png
  :width: 600
  :alt: Create Module options window


.. |Template| image:: postchain-ide-with-template.png
  :width: 600
  :alt: The IDE with a template

.. |Success| image:: kiev-success.png
  :width: 600
  :alt: The IDE shows Run Succesfully

.. |Main Settings Button| image:: main-settings-button.png
  :width: 600
  :alt: Main settings button

.. |Set Rell Version| image:: update-rell-version.png
  :width: 600
  :alt: Choose Current Rell Cloude IDE Url to use latest Rell version
