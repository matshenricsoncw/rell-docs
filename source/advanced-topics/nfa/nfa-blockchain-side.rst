================
Blockchain Side
================

.. contents:: Table of Contents

-----

Integrating NFA Module
----------------------

For project setup, please refer to :doc:`Project setup for FT3</advanced-topics/ft3/ft3-project-setup>`.

From the cloned NFA repository, copy ``rell/src/lib/nfa`` directory into your project's ``lib`` directory (same location as ``ft3`` lib). Include the module in your project main file (``main.rell``):

.. code-block:: rell

  include "config/ft3_config";
  include "lib/ft3/ft3_xc_basic_dev";
  include "lib/nfa/core";
  include "lib/nfa/ft3/ownership";

----

The remaining of This section cover features of NFA module (i.e. code snippets in this section are quoted from above repository).

Creating nfa
------------

``nfa`` can be created via built-in **operation** (``nfa`` creation from client side) or **functions** (on blockchain side).

We do not recommend exposing NFA creation to client side. It it preferable to create dapp-managed operations (with own validation and authorization) and call functions to create ``nfa`` from these operations.

Functions
~~~~~~~~~

Create a simple nfa class:

.. code-block:: rell

  function createNFAClass(
    name,
    desc: text,
    max_entities: integer
  ): nfa

Create nfa with some properties:

.. code-block:: rell

  struct component_req {
    is_required: boolean = false;
    is_mutable: boolean = true;
  }

  struct component_def {
    component;
    component_req;
  }

  createNFA(
    name,
    desc: text,
    max_entities: integer,
    components: map<name, component_def> // map of component_structures with the name of the property as a key
  ): nfa

... or using tuple:

.. code-block:: rell

  easyCreateNFA(
    name,
    desc: text,
    max_entities: integer,
    components: map<
        name,
        (
          byte_array, // component id
          boolean,    // is_required
          boolean     // is_mutable
        )
      >
  ): nfa

Operations
~~~~~~~~~~

.. code-block:: rell

  operation easy_create_nfa(
    name,
    desc: text,
    max_entities: integer,
    propertiesI: list<component_iface>
  )

  struct component_iface {
    id: byte_array;
    name;
    blockchain_rid: byte_array;
    type: supported_types.type;
    extra: gtv;
  }

.. note::
  ``easy_create_nfa`` will create nfa, properties and also components described in ``propertiesI`` if they don't exist.

If the component already exists, we can use ``register_nfa``:

.. code-block:: rell

  // internally calls createNFA() function
  operation register_nfa(
    name,
    desc: text,
    max_entities: integer,
    properties: map<name, component_def>
  )

Creating component
-------------------

Functions
~~~~~~~~~

.. code-block:: rell

  function createComponent(
    name: text,
    type: supported_types.type,
    extra: gtv? // extra_*_component_structure encoded in gtv
  ): component

``ensureComponent`` return the component if the specified component_id exists, otherwise create a new component with the given parameters:

.. code-block:: rell

  function ensureComponent(
    component_name: text,
    component_bc_rid: byte_array, // will use current blockchain_rid if creating component
    component_type: supported_types.type, // type supported by this component
    extra: gtv // extra_*_component_structure encoded in gtv
  ): component


Operations
~~~~~~~~~~

.. code-block:: rell

  // internally call createComponent
  operation create_component_structure(new_c: component_iface)

Handling entitee
----------------

Operations
~~~~~~~~~~

Create one entity programmatically. The ``idx`` is automatically generated.

Note that ``new_id`` must be specified by creator, this is done on purpose to help the frontend software to keep track of different entitees

.. code-block:: rell

  operation create_entitee(
    nfa_name: text,
    new_id: byte_array,
    properties: list<property_value>
  )

  struct property_value {
    name;       // name of the property
    value: gtv; // value for named property
  }

Queries
~~~~~~~

Returns the entity by id and its properties:

.. code-block:: rell

  struct entity_obj {
    id: byte_array;
    idx: integer;
    nfa_id: byte_array;
    nfa_name: text;
  }

  struct entity_description {
    entitee: entity_obj;
    properties: map<text, gtv>;
  }

  query get_entity (
    entity_id: byte_array
  ): entity_description

Functions
~~~~~~~~~

Create:

.. code-block:: rell

  function createEntitee(
    nfa,
    id: byte_array,
    entity_components: map<text, gtv> // property name with value encoded in gtv.
  ): entitee

Read:

.. code-block:: rell

  getEntitee(
    e: entitee
  ): entity_description

Update:

.. code-block:: rell

  modifyEntitee(
    auth_descriptor_id: byte_array?,
    entitee,
    entity_properties: map<name, gtv?>
  )

- ``auth_descriptor_id`` is optional (e.g. the entitee does not have an owner). If the entitee has an owner, the function will check for such authorization from FT3

- ``entity_properties: map<name, gtv?>`` is the map of properties to modify. If instead of gtv, ``null` is specified, the property will be removed from the entity (``is_required`` must be ``false``).

Handling property
-----------------

Operations
~~~~~~~~~~

.. code-block:: rell

  operation update_property(
    entitee_id: byte_array,
    property_name: text,
    value: gtv
  )

Queries
~~~~~~~

.. code-block:: rell

  query get_property_value(
    entity_id: byte_array,
    property_name: text
  )

Functions
~~~~~~~~~

.. code-block:: rell

  function modifyProperty(
    e: entitee,
    property,
    gtvValue: gtv
  )

.. code-block:: rell

  function removeProperty(
    e: entitee,
    property
  )

.. code-block:: rell

  function getPropertyValue(
    e: e.entitee,
    property_name: text
  ): gtv

Ownership
---------

Ownership is an integration with FT3 module. It allows to express the ownership of one ``ft3.account`` of one or more ``nfa.entitee``.

.. code-block:: rell

  entity ownership {
    key entitee, account: ft3.acc.account;
  }

Queries
~~~~~~~

Returns the account.id of the entitee's owner:

.. code-block:: rell

  query get_owner(
    e: e.entitee
  )

Functions
~~~~~~~~~

A "require" function that check if provided ``auth_descriptor_id`` is the owner of the entitee. If ``auth_descriptor_id`` is null it mean the entitee is not expected to have an owner:

.. code-block:: rell

  function require_ownership(
    e: entitee,
    auth_descriptor_id: byte_array?
  )

Set the owner of one entitee to the specified account. Can be used only if the entitee has no owner:

.. code-block:: rell

  function setOwner(
    e: entitee,
    account: ft3.acc.account
  )

.. note::

  ``setOwner`` performs no check over authenticity (so anybody could assign an entitee to any account). The check should be beared inside the operation that calls such function.

Remove the owner from one entitee, transaction needs to be signed by the current owner.

.. code-block:: rell

  function removeOwner(
    e: entitee,
    account: ft3.acc.account,
    auth_descriptor_id: byte_array
  )
