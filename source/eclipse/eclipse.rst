===========
Eclipse IDE
===========

Installation
============

Prerequisites
-------------

* Java 8+.
* To be able to run a Postchain node: PostgreSQL 10.

Using a full Rell Eclipse IDE bundle
------------------------------------

Rell Eclipse IDE can be downloaded here: https://www.chromia.dev/rell-eclipse/index.html.

Download an archive for your OS.

* Linux: unpack, run ``eclipse``
* Windows: unpack, run ``eclipse.exe``
* MacOS: open the DMG image, run or install Eclipse.


Adding Rell plugin to an existing Eclipse IDE
---------------------------------------------

If you already have an Eclipse IDE (for example, Eclipse IDE for Java), the Rell plugin can be added to it.

#. Go to the menu **Help** - **Install New Software...**
#. In the Install dialog, click **Add...**, then type:

   * Name: Rell
   * Location: https://www.chromia.dev/rell-eclipse/update

   .. image:: images/install_add_site.png

#. Rell shall appear in the list. Select it and click **Next >**, then **Finish**.

   .. image:: images/install.png
#. When seeing a warning "You are installing software that contains unsigned content", click **Install anyway**.
#. Click **Restart Now** when asked to restart Eclipse IDE.
#. Switch to the Rell perspective. Menu **Window** - **Perspective** - **Open Perspective** - **Other...**, choose **Rell**.

   .. image:: images/menu_perspective_crop.png

Next step is to create a Rell project (see `Hello World Program`_).

How to update the Rell plugin
-----------------------------

When a new version of the Rell plugin is released, it has to be updated in Eclipse. If Rell update URL has already been configured, Eclipse will check for
updates automatically once in a while, and show a message when a plugin can be updated.

To manually check for updates:

#. Menu: **Help** - **Check for Updates**.
#. If it shows that a new version of Rell plugin is available, install it.

If "No updates found" message is shown, check that Rell update site is set up.

.. image:: images/update_no_updates_found.png

#. Click **available software sites** link in the message dialog.
#. If there is no Rell in the list, click **Add...** to add it.

   .. image:: images/update_available_software_sites.png
#. Specify:

   * Name: Rell
   * Location: https://www.chromia.dev/rell-eclipse/update

   and click **Add**.

If Rell update site is in the list, but "No updates found" message is shown, try to reload the site:

#. Click **available software sites** link in the message dialog.
#. Click **Reload**.

If still no updates are shown, your Rell plugin must be already up-to-date.

.. _database-setup:

Database Setup
--------------

Rell requires PostgreSQL 10 to be installed and set up. The IDE can work without it, but will not be able to run a node.
A console app or a remote postchain app can be run without a database, though.

Default database configuration for Rell is:

* database: ``postchain``
* user: ``postchain``
* password: ``postchain``

Ubuntu (Debian)
~~~~~~~~~~~~~~~~

Install PostgreSQL:

.. code-block:: none

    sudo apt-get install postgresql

Prepare a Rell database:

.. code-block:: none

    sudo -u postgres psql -c "CREATE DATABASE postchain;" -c "CREATE ROLE postchain LOGIN ENCRYPTED PASSWORD 'postchain'; GRANT ALL ON DATABASE postchain TO postchain;"

MacOS
~~~~~~~~~~~~~~~~~

Install PostgreSQL:

.. code-block:: none

    brew install postgresql
    brew services start postgresql
    createuser -s postgres

Prepare a Rell database:

.. code-block:: none

    psql -U postgres -c "CREATE DATABASE postchain;" -c "CREATE ROLE postchain LOGIN ENCRYPTED PASSWORD 'postchain'; GRANT ALL ON DATABASE postchain TO postchain;"


Hello World Program
===================

#. Switch to the Rell perspective, if not done already. Menu: **Window** - **Perspective** - **Open Perspective** - **Other...**, choose **Rell**.
#. Create a project:

   * Menu **File** - **New** - **Rell Project**.
   * Enter a project name ``test`` and click **Finish**.

   .. image:: images/new_template_project_dialog_crop.png

   A Rell project with a default directory structure shall be created.
#. Create a Rell file:

   * Right-click on the ``src`` folder and choose **New** - **File**.
   * Enter a file name ``test.rell`` and click **Finish**.
#. Write the code in the editor:

   ::

       function main() {
           print('Hello, World!');
       }


   Save: manu **File** - **Save** or CTRL-S (⌘S).

#. Run the program: right-click on the editor and choose **Run As** - **Rell Console App**.

   .. image:: images/run_console_app_menu_crop.png
#. The output "Hello, World!" must be shown in the Console view.

   .. image:: images/run_console_app_output.png

IDE Overview
=============

Eclipse IDE window consists of different views. Every view has its own tab. By default, Rell IDE has following views:

* **Project Explorer** - shows projects and their directory trees.
* **Problems** - shows compilation warnings and errors.
* **Console** - console output (when running programs).
* **Outline** - shows the structure of a selected Rell file.

.. image:: images/ide_empty.png

Running Applications
====================

Right-click on a file (or an editor) and choose **Run As**. Run options available for the file will be shown.

.. image:: images/run_menu_crop.png

Alternatively, use keyboard shortcut CTRL-F11 (⇧⌘F11).

.. image:: images/run_dialog_crop.png

Rell Console App
----------------

Executes a ``*.rell`` file as a stand-alone console program, not as a module in a Postchain node.

The program must contain a function (or operation, or query) called ``main``, which will be the entry point. The output is displayed in the Console view.

The name of the main function and its arguments can be specified in a `run configuration <run-configurations_>`_.


Database connection
~~~~~~~~~~~~~~~~~~~

By default, the program is executed without a database connection, and an attempt to perform a database operation will result in a run-time error.

To run a console app with a database connection, there must be a file called ``console-db.properties``, ``db.properties`` or ``node-config.properties``
in the directory of the Rell file or in the ``rell/config`` directory of the project. The file shall contain database connection settings. For example:

.. code-block:: none

    database.driverclass=org.postgresql.Driver
    database.url=jdbc:postgresql://localhost/postchain
    database.username=postchain
    database.password=postchain
    database.schema=rell_app

.. image:: images/run_console_app_db_properties.png

When running a console app with a database connection, tables for defined classes and objects are created on start-up. If a table already exists, missing
columns are added, if necessary.

How to prepare a database is described in the `Database Setup <database-setup_>`_ section.


Rell Simple Postchain App
-------------------------

Starts a Postchain node running the given Rell module.

A database connection and other standard Postchain node properties must be specified in a ``node-config.properties`` file.
The file is searched in the directory of the selected Rell file and then in the ``rell/config`` directory.

If the properties file is not found, the IDE offers to create a default one; click **Yes**.

.. image:: images/run_simple_app_node_config_not_found.png

If a database configuration which is different from the default for Rell (``postchain/postchain/postchain`` as database/user/password) has to be used, specify it in the created file.
Then run the app again.

Rell program output and Postchain node log is shown in the Console view. To stop a node, click the *Terminate* (red square) icon in the Console view.

.. image:: images/run_simple_app_output.png

One can use ``curl`` to call a query from the running node:

.. code-block:: none

    curl http://localhost:7740/query/0123456789ABCDEF0123456789ABCDEF0123456789ABCDEF0123456789ABCDEF -X POST -d '{"type":"q"}'

By default, the blockchain RID is 0123456789ABCDEF0123456789ABCDEF0123456789ABCDEF0123456789ABCDEF.
It can be changed in a `run configuration <run-configurations_>`_.

Rell Postchain App
------------------

Starts a Postchain node with a configuration written in the `Run.XML <../runxml.html>`_ format.
To use this option, right-click on a ``*.xml`` file, not on a ``*.rell`` file.

Using run.xml allows to run multiple blockchains in one Postchain node.

Example of a minimal run.xml:

.. code-block:: xml

    <run>
        <nodes>
            <config>
                database.driverclass=org.postgresql.Driver
                database.url=jdbc:postgresql://localhost/postchain
                database.username=postchain
                database.password=postchain
                database.schema=rell_app

                activechainids=1

                api.port=7740
                api.basepath=

                messaging.privkey=3132333435363738393031323334353637383930313233343536373839303131
                messaging.pubkey=0350fe40766bc0ce8d08b3f5b810e49a8352fdd458606bd5fafe5acdcdc8ff3f57

                node.0.id=node0
                node.0.host=127.0.0.1
                node.0.port=9870
                node.0.pubkey=0350fe40766bc0ce8d08b3f5b810e49a8352fdd458606bd5fafe5acdcdc8ff3f57
            </config>
        </nodes>
        <chains>
            <chain name="user" iid="1">
                <config>
                    <app module="main" />
                </config>
            </chain>
        </chains>
    </run>

.. image:: images/run_postchain_app_output_runxml.png


Rell Simple Postchain Test
--------------------------

Runs tests written in XML format, same as used by the *Web* IDE. This run option is available for XML files ``*_test.xml``.
For a file ``X_test.xml`` there must be a file called ``X.rell`` in the same directory. The tests defined in the XML file will be run against the Rell file.
Results are printed to the Console view.

Example of a test:

.. code-block:: xml

    <test>
        <block>
            <transaction>
                <signers>
                    <param type="bytea" key="Alice" />
                </signers>
                <operations>
                    <operation name="insert_city">
                        <string>Kiev</string>
                    </operation>
                </operations>
            </transaction>
        </block>
    </test>

And a corresponding ``.rell`` file:

::

    class city { key name; }

    operation insert_city (name) {
        log('insert_city', name);
        create city (name);
    }


.. image:: images/run_simple_test_output.png


Clear Database
--------------

This option is available for database properties files, ``*.properties`` (for instance, ``node-config.properties``).
Drops all tables (and stored procedures) in the database.

.. image:: images/clear_database_menu_crop.png


Run with a Keyboard
-------------------

By default, CTRL-F11 (⇧⌘F11) shortcut runs the file of the active editor. It can be configured to run the last launched application instead, which may be more convenient,
as there is no need to choose an application type. Go to the menu **Window** - **Preferences** (macOS: **Eclipse** - **Preferences**), then **Run/Debug** - **Launching**.
In the **Launch Operation** box, choose **Always launch the previously launched application**.

.. image:: images/preferences_launching_ed.png


Running Multiple Apps
---------------------

It is possible to run multiple applications (e. g. multiple nodes) simultaneously.
For example, one can define two `Run.XML <../runxml.html>`_ configuration files that use the same Rell module, but different ports and database schemas.

The output of all running applications will be shown in the Console view, but on different pages.
It is possible to switch between the consoles of different applications using a button or a dropdown list.

.. image:: images/run_console_switch_ed.png

.. _run-configurations:

Run Configurations
------------------

To run an application, Eclipse IDE needs a run configuration, which contains different properties, like the name of the main function, arguments or
blockchain RID.
When running an application via the **Run As** context menu, the IDE automatically creates a run configuration with default settings if it does not exist.

To change a run configuration, go to the menu **Run** - **Run Configurations...**. The last launched application will be selected.
Change the settings and click either **Apply** or **Run** to save the changes.

.. image:: images/run_configurations_console_dialog.png

Features of the IDE
===================

Outline
-------

When a Rell editor is open, the structure of its file (tree of definitions) is shown in the Outline view (which is by default on the right side of the IDE).

.. image:: images/outline_view_2.png

There is also a Quick outline window, which is activated by the CTRL-O (⌘O) shortcut. Type a name to find its definition in the file.

.. image:: images/outline_popup_ed.png
.. image:: images/outline_popup_4.png


Problems View
-------------

Problems view shows compiler warnings and errors found in all projects in the IDE. The list is updated when saving a file.

.. image:: images/problems_view.png


Global Text Search
------------------

Press CTRL-H (⌘H) to open the Search dialog. It allows to search for a string in all files in the IDE. Select the **File Search** tab, enter the text to search for
and file name pattern.

.. image:: images/search_dialog.png

Results are shown in the Search view, as a tree.

.. image:: images/search_results.png


Git
-------------

Git operations are available via a context menu: right-click on a file and choose **Team**.

.. image:: images/git_menu_ed.png

To commit file(s), click **Add to Index**, then **Commit..**.

File icon in the Project Explorer indicates whether the file is a new, changed or an unmodified file.

.. image:: images/git_files_ed.png

Changed files do not look very nice. To change the way how they are displayed, go to the **Window** - **Preferences** (macOS: **Eclipse** - **Preferences**) menu, then
**Team** - **Git** - **Label Decorations**:

* on the **Text Decorations** tab: delete the ">" character in all text fields

   .. image:: images/git_preferences_text_decorations_ed.png
* on the **Icon Decoartions** tab: check the **Dirty resources** checkbox

   .. image:: images/git_preferences_icon_decorations_ed.png
* click **Apply and Close**

Now files look much better:

.. image:: images/git_files_4_ed.png

Spaces vs. Tabs
---------------

Eclipse uses tabs instead of spaces by default. It is recommented to use spaces. A few settings have to be changed:

#. Open the Preferences dialog: menu **Window** - **Preferences** (macOS: **Eclipse** - **Preferences**).
#. Type "tabs" in the search box.
#. Go to **General** - **Editors** - **Text Editors** and check **Insert spaces for tabs**.

   .. image:: images/preferences_text_editors_ed.png
#. Go to **XML** - **XML Files** - **Editor**, select **Indent using spaces**, specify **Indentation size**: 4.

   .. image:: images/preferences_xml_editor_ed.png


Miscellaneous
-------------

* To show or hide line numbers: right-click on the left margin of an editor, click **Show Line Numbers**.

   .. image:: images/menu_show_line_numbers_ed.png

* To comment a code fragment, select it and press CTRL-/ (⌘/).

* Activate the *Link with Editor* icon, and the IDE will automatically select a file in the Project Explorer when its editor is focused.

   .. image:: images/link_with_editor_ed.png

* CTRL-SHIFT-R (⇧⌘R) invokes the Open Resource dialog, which allows to search project files by name. Glob patterns (with ``*`` and ``?``) are supported.

   .. image:: images/open_resource_dialog_ed.png

* The *Show Whitespace Characters* (paragraph) icon in the main toolbar allows to distinguish tabs from spaces.

   .. image:: images/show_whitespace_ed.png

* To see the full list of Eclipse keyboard shortcuts, press CTRL-SHIFT-L (⇧⌘L). Some shortcuts are for the Java editor, and do nothing in Rell.

   .. image:: images/keyboard_shortcuts_ed.png

Keyboard shortcuts
------------------

Most useful keyboard shortcuts (subjectively):

========================================================================= =============== =============
Action                                                                    Linux/Windows   macOS
========================================================================= =============== =============
Run the file shown in the active editor                                   CTRL-F11        ⇧⌘F11
|
Show the New wizard                                                       CTRL-N          ⌘N
Show the New menu                                                         ALT-SHIFT-N     ⌥⌘N
Save the current file                                                     CTRL-S          ⌘S
Close the active editor tab                                               CTRL-W          ⌘W
Close all editor tabs                                                     CTRL-SHIFT-W    ⇧⌘W
|
Quick outline                                                             CTRL-O          ⌘O
Find text in the active editor                                            CTRL-F          ⌘F
Globally search for the selected text fragment                            CTRL-ALT-G      ⌥⌘G
Show global text search dialog                                            CTRL-H          ^H
Find a file by name                                                       CTRL-SHIFT-R    ⇧⌘R
|
Go to a line number                                                       CTRL-L          ⌘L
Go to a previous location                                                 ALT-Left        ⌥⌘←
Go to a next location (can be used after Alt-Left)                        ALT-Right       ⌥⌘→
Go to the last edit location                                              CTRL-Q          ⌘Q
|
Comment the selected code fragment with ``//``                            CTRL-/          ⌘/
Convert selected text to upper case                                       CTRL-SHIFT-X    ⇧⌘X
Convert selected text to lower case                                       CTRL-SHIFT-Y    ⇧⌘Y
|
Show the full list of keyboard shortcuts                                  CTRL-SHIFT-L    ⇧⌘L
========================================================================= =============== =============
